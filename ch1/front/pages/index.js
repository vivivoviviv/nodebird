import React from 'react';
// Next에선 import react 써줄 필요없다. 다만 결국 뒤에 추가되는 거 때문에 써야한다.
import PostForm from '../components/PostForm';
import PostCard from '../components/PostCard';

const dummy = {
    isLoggedIn: true,
    imagePaths : [],
    mainPosts : [{
        User: {
            id: 1,
            nickname: 'sweep',
        },
        content: '첫 번째 게시글',
        img: '',
    }],
}

const Home = () => {
    return (
        <div>
            {dummy.isLoggedIn && <PostForm />}
            {dummy.mainPosts.map((c) => {
                return (
                    <PostCard key={c} post={c} />
                );
            })}
        </div>
    );
};

export default Home;
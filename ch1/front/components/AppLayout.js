import React from 'react';
import Link from 'next/link';
import PropTypes from 'prop-types';
import { Menu, Input, Button, Col, Row } from 'antd';
import LoginForm from './LoginForm';
import UserProfile from './UserProfile';

const dummy = {
    nickname : "sweep",
    Post : [],
    Followings: [],
    Followers: [],
    isLoggedIn: false,
}

const AppLayout = ({ children }) => {
    return (
        <div>
            <Menu mode="horizontal">
                <Menu.Item key="home"><Link href="/"><a>노드버드</a></Link></Menu.Item>
                <Menu.Item key="profile"><Link href="/profile"><a>프로필</a></Link></Menu.Item>
                <Menu.Item key="mail">
                    <Input.Search enterButton style={{ verticalAlign: 'middle' }} />
                </Menu.Item>
            </Menu>
            <Link href="/signup"><a><Button>회원가입</Button></a></Link>
            <Row gutter={8}>
                <Col xs={24} md={6}>
                    {dummy.isLoggedIn ? 
                    <UserProfile />
                        :
                    <LoginForm />
                    }
                </Col> 
                <Col xs={8} md={12}>
                    { children }
                </Col>
                <Col xs={8} md={6}>
                    <Link href="https://bitbucket.org/vivivoviviv/nodebird"><a target="_blank">Made By Sweep Hee</a></Link>
                </Col>
            </Row>
        
        </div>
    );
};

// antd xs={24}  24가 한줄 꽉. 4/4인 것.

AppLayout.propTypes = {
    children : PropTypes.node
};

export default AppLayout;
React로 NodeBird SNS 만들기
===


1. 페이지에서 중복되는 부분 리렌더링 되지 않게 하기.
---
```

const NodeBird = ({ Component }) => {
    return (
        <>
            <Head>
                <title>NodeBird</title>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/antd/3.16.2/antd.css" />
            </Head>
            <AppLayout>
                <Component />
            </AppLayout>
        </>
    );
};

/*
_app.js 파일 생성해서 중복되는 부분을 작성해주고 
페이지가 들어갈 부분에 <Component /> 를 정의해준다.
페이지는 pages 폴더안에 만든다. (next가 알아서 인식해줌)
함수 인자값에 { Component } 를 넘겨야한다.
*/

```

 - Component안에 리렌더링이 되는 요소가 있을 때, _app.js에서 정의한 부분은 리렌더링 되지 않지만 Component부분은 다시 리렌더링 된다.
 - 이걸 방지해주는 방법은 있지만 시간이 걸리고 과한 최적화기 때문에 굳이 해주진 않는다.
 - 필요하다면 [React로 NodeBird SNS만들기] 2-1 참고.
 - 또, Form들은 보통 다 분리해서 컴포넌트로 추가하는게 좋다.(폼은 보통 다 state를 쓰기때문에 값 입력 시 onChange이벤트 때문에 계속 리렌더링 되기때문)
#  
#  
#  
## 2. 함수 컴포넌트에 인자값 경고 없애기.
---
```
import PropTypes from 'prop-types';

.
.
.

함수컴포넌트명.propTypes = {
    Component: PropTypes.elementType
};

// npm i prop-types 설치 후 해당코드 선언. 

```
 - 이걸 해주는 이유는 정확히는 모르지만 함수컴포넌트 인자에 경고가 떠서    props를 정해줘야하는데(string인지 int인지)
 - 그걸 정해주는 거 인듯함. (프로그램의 안정성을 높일 수 있음)
 - 뭘로 정하는지는 아직 개념이 확실치 않음.
 - 타입은 www.npmjs.com/package/prop-types 에 들어가서 확인!!
 - 여기 Component인자같은경우는 <Component />가 있어서 element를 사용해야하는데 아직 jsx로 선언되지 않았기때문에 elementType으로 정의해야 한다고 함.
 - node란 jsx에서 컴포넌트, 태그 들 등등 <></>안에 정의된 것들 전체를 뜻함. 쉽게말해서 렌더링될 수 있는 모든 것.
#  
#  
#  
## 3. Custom Hook 다른 경로에서 쓸 수 있게 만들기.
---
```

 // 함수 컴포넌트 밖에 Custom Hook 정의.
 export const useInput = (initValue = null) => {
    const [value, setter] = useState(initValue);
    const handler = (e) => {
        setter(e.target.value);
    };
    return [value, handler];
};

---

// Import 할 곳에서 정의
import {useInput} from '경로';
// export const 라서 {useInput} 이런식으로 정의해주어야 함.

```
#  
#  
#  
## 4. React 조건문 &&
---
```

 {dummy.isLoggedIn && <div>조건에 맞을 시 렌더링~</div>}

```
  - if 조건문과 거의 동일.
  - 맞으면 실행하고 아니면 렌더링 하지 않게-화면구성할때- 사용
#  
#  
#  
## 5. React 반복문
---
```

  {dummy.imagePaths.map((v, i) => {
        return (
            <div key={v} style={{ display: 'inline-block' }}>
                <img src={'http://localhost:3065/'+v} style={{ width: '200px' }} alt={v} />
                <div>
                    <Button>제거</Button>
                </div>    
            </div>
        )
    })}

```
 - (아직 정확히 이해 못 함.)
 - .map((v, i)) 를 사용해서 i만큼 반복한다고 이해했음.
 - 만약 return 안의 부분을 컴포넌트로 불러온다면 key와 props를 넘겨준다.  <Component key={c} 프롭스값={c} />, 또 컴포넌트.js 파일에선 props인자부분에 넘어올 값을 적어준다. { props }
 - (추후에 수정)
